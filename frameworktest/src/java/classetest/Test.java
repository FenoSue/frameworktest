/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package classetest;

import annotations.AnnotationMethod;
import classes.ModelView;
import java.util.ArrayList;

/**
 *
 * @author nyamp
 */
public class Test {
    int id;
    String nom;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }
    
    @AnnotationMethod(url="listTest")
    public ModelView listTest() {
        ModelView modelview = new ModelView();
        ArrayList<Test> littest = new ArrayList();
        Test test1 = new Test();
        test1.setId(1);
        test1.setNom("Test 1");
        Test test2 = new Test();
        test2.setId(2);
        test2.setNom("Test 2");
        littest.add(test1);
        littest.add(test2);
        modelview.setUrl("list.jsp");
        modelview.setData("test", littest);
        return modelview;
    }
}
