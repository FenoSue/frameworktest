/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package classetest;

import annotations.AnnotationMethod;
import classes.ModelView;

/**
 *
 * @author popotaaaaaaa
 */
public class Test2 {
    int id;
    String url;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
    
    @AnnotationMethod(url="create")
    public ModelView create(){
        ModelView modelview = new ModelView();
        Test2 test = new Test2();
        test.setId(this.getId());
        test.setUrl(this.getUrl());
        modelview.setUrl("create.jsp");
        modelview.setData("test2", test);
        return modelview;
    }
}
