/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package testsecond;

import annotations.AnnotationMethod;
import classes.ModelView;
import dao.Insert;
import dao.Select;
import java.sql.Date;
import java.util.Vector;

/**
 *
 * @author popotaaaaaaa
 */
public class Testsecond {
    int id;
    String nom;
    Date naissance;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public Date getNaissance() {
        return naissance;
    }

    public void setNaissance(Date naissance) {
        this.naissance = naissance;
    }
    
    @AnnotationMethod(url = "createTestsecond")
    public ModelView createTestsecond() throws Exception {
        ModelView modelview = new ModelView();
        Testsecond test = new Testsecond();
        test.setNom(this.getNom());
        test.setNaissance(this.getNaissance());
        Vector list = new Vector();
        list.add(test.getNom());
        list.add(test.getNaissance());
        new Insert().insert(null, new Testsecond(), list);
        modelview.setUrl("index.jsp");
        modelview.setData("testsecond", test);
        return modelview;
    }
    
    @AnnotationMethod(url = "readTestsecond")
    public ModelView readTestsecond() throws Exception {
        ModelView modelview = new ModelView();
        Object[] listtestsecond = new Select().select(new Testsecond(), null);
        Testsecond[] test = new Testsecond[listtestsecond.length];
        int i=0;
        for(i=0; i<test.length; i++) {
            test[i] = (Testsecond) listtestsecond[i];
        }
        modelview.setUrl("listtest.jsp");
        modelview.setData("testsecond", test);
        return modelview;
    }
    
    @AnnotationMethod(url = "redirect")
    public ModelView redirect() {
        ModelView modelview = new ModelView();
        modelview.setUrl("index.jsp");
        modelview.setData("testsecond", null);
        return modelview;
    }
}
