/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package outil;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

/**
 *
 * @author nyamp
 */
public class Outil {
    public int getTailleColoumn(Connection connection, ResultSet resultset, PreparedStatement preparedstatement, Object objet) 
            throws Exception {
        int taillecoloumn = 0;
        try {
            String tablename = objet.getClass().getSimpleName();
            String sql = "select * from " + tablename;
            preparedstatement = connection.prepareStatement(sql);
            resultset = preparedstatement.executeQuery();
            taillecoloumn = resultset.getMetaData().getColumnCount();
        }      
        catch(Exception e) {
            System.out.println(e);
            throw e;
        }
        return taillecoloumn;
    }

    public static String upperCaseFirst(String val) {
        char[] arr = val.toCharArray();
        arr[0] = Character.toUpperCase(arr[0]);
        return new String(arr);
    }
    
    public static String upperCaseSecond(String val) {
        char[] arr = val.toCharArray();
        arr[2] = Character.toUpperCase(arr[0]);
        return new String(arr);
   }
    
    public int getNombreResultat(ResultSet resultset) throws Exception {
        int size = 0;
        resultset.last();
        size = resultset.getRow();
        resultset.beforeFirst();
        return size;
    }
    
    public Method getMethodeGet(Object objet, Field field) throws NoSuchMethodException, Exception{
        Method methodget = null;
        Class classe = objet.getClass();
        try {
            String fieldname = new Outil().upperCaseFirst((field.getName()));
            methodget = classe.getMethod("get" + fieldname);
        }
        catch(SecurityException exception1) {
            try {
                String fieldname = field.getName();
                methodget = classe.getMethod("get" + fieldname);              
            }
            catch(NoSuchMethodException | SecurityException exception2) {
                try{
                    String fieldname = field.getName().toLowerCase();
                    methodget = classe.getMethod("get" + fieldname);
                }
                catch(SecurityException exception3){
                    throw exception3;
                }
            }
        }
        return methodget;
    }
    
    public Method getMethodSet(Object objet,Field field) throws NoSuchMethodException {
        Method methodset = null;
        Class classe = objet.getClass();
        try {
            String fieldname = new Outil().upperCaseFirst((field.getName()));
            methodset = classe.getMethod("set" + fieldname, field.getType());
        }
        catch(SecurityException exception1) {
            try {
                String fieldname = field.getName();
                methodset = classe.getMethod("set" + fieldname, field.getType());            
            }
            catch(NoSuchMethodException | SecurityException exception2) {
                try{
                    String fieldname = field.getName().toLowerCase();
                    methodset = classe.getMethod("set" + fieldname, field.getType());
                }
                catch(SecurityException exception3){
                    throw exception3;
                }
            }
        }
        return methodset;
    }
    
    public boolean isCharacter(String fieldtypename) {
        boolean result;
        if(fieldtypename.equalsIgnoreCase("java.lang.String") == true) {result=true; }
        else if(fieldtypename.equalsIgnoreCase("java.sql.Date") == true) { result=true; }
        else if(fieldtypename.equalsIgnoreCase("java.util.Date") == true) { result=true; }
        else if(fieldtypename.equalsIgnoreCase("java.sql.Time") == true) { result=true; }
        else { result=false; }
        return result;
    }
}
