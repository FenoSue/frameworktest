/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import connexion.Connexion;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import outil.Outil;

/**
 *
 * @author nyamp
 */
public class Select {
    public Object[] getselect(Object objet,String requette,Connection connection) throws Exception {
        int i=0;
        int j=0;
        Outil outil = new Outil();
        int taillemethodsets=0;
        Method method = null;
        Object[] objetreturn = null;
        PreparedStatement preparedstatement = null;
        ResultSet resultset = null;
        if(connection==null) {
            connection = new Connexion().getConnection();
        }
        try {
            preparedstatement = connection.prepareStatement(requette, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
            resultset = preparedstatement.executeQuery();
            
            int nombreresultat = new Outil().getNombreResultat(resultset);
            
            int taillecoloumn = new Outil().getTailleColoumn(connection, resultset, preparedstatement, objet);
            
            objetreturn = new Object[nombreresultat];
            
            String[] namecoloumn = new String[taillecoloumn];
            String[] typecoloumn = new String[taillecoloumn];
            
            for(i=0; i<taillecoloumn; i++) {
                namecoloumn[i] = resultset.getMetaData().getColumnName(i+1);
                typecoloumn[i] = resultset.getMetaData().getColumnTypeName(i+1);
            }
            
            Field[] fields = objet.getClass().getDeclaredFields();
            int taillefield = fields.length;
                    
            int indice=0;
            
            while(resultset.next()) {
                Object temporaire=objet.getClass().newInstance();
                for(j=0; j<taillecoloumn; j++) {
                    for(i=0; i<taillefield; i++) {
                        if(typecoloumn[j].equalsIgnoreCase("varchar") == true && namecoloumn[j].equalsIgnoreCase(fields[i].getName().toUpperCase()) == true) {
                            String result = resultset.getString(j+1);
                            method = outil.getMethodSet(objet, fields[i]);
                            method.invoke(temporaire, result);
                        }
                        else if(typecoloumn[j].equalsIgnoreCase("serial") == true && namecoloumn[j].equalsIgnoreCase(fields[i].getName().toUpperCase()) == true) {
                            int result = resultset.getInt(j+1);
                            method = outil.getMethodSet(objet, fields[i]);
                            method.invoke(temporaire, result);
                        }
                        else if(typecoloumn[j].equalsIgnoreCase("number") == true && namecoloumn[j].equalsIgnoreCase(fields[i].getName().toUpperCase()) == true) {
                            int result = resultset.getInt(j+1);
                            method = outil.getMethodSet(objet, fields[i]);
                            method.invoke(temporaire, result);
                        }
                        else if(typecoloumn[j].equalsIgnoreCase("int4") == true && namecoloumn[j].equalsIgnoreCase(fields[i].getName().toUpperCase()) == true) {
                            int result = resultset.getInt(j+1);
                            method = outil.getMethodSet(objet, fields[i]);
                            method.invoke(temporaire, result);
                        }
                        else if(typecoloumn[j].equalsIgnoreCase("date") == true && namecoloumn[j].equalsIgnoreCase(fields[i].getName().toUpperCase()) == true) {
                            Date result = resultset.getDate(j+1);
                            method = outil.getMethodSet(objet, fields[i]);
                            method.invoke(temporaire, result);
                        }    
                    }    
                }
                objetreturn[indice] = temporaire;
                indice++;
            }
        }
        catch(Exception e) {
            System.out.println(e);
            throw e;
        }
        finally {
            if(resultset!=null) { resultset.close(); }
            if(preparedstatement!=null) { preparedstatement.close(); }
            if(connection!=null) { connection.close(); }
        }
        return objetreturn;
    }
    
    public Object[] getselect(Object objet,Connection connection) throws Exception {
        int i=0;
        Object[] returnobjet = null;
        String tablename = objet.getClass().getSimpleName();
        String requette = "select * from "+tablename;
        returnobjet = this.getselect(objet, requette, connection);
        return returnobjet;
    }
    
    public Object[] select(Object objet,Connection connection) throws Exception {
        return getselect(objet, connection);
    }
    
}
