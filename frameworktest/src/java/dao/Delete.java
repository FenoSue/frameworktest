/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import connexion.Connexion;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author nyamp
 */
public class Delete {
    public void delete(Object obj, int id_objet) throws Exception {
        Connection connection = null;
        PreparedStatement statement = null;
        int delete = 0;
        
        try {
            connection = new Connexion().getConnection();
            String tablename = obj.getClass().getSimpleName();
            String sql = "delete from " + tablename + " where id = ?";
            statement = connection.prepareStatement(sql);
            statement.setInt(1, id_objet);
            delete = statement.executeUpdate();
        }
        
        catch(Exception e) {
            System.out.println(e);
            throw e;
        }
        
        finally {
            statement.close();
            connection.close();
        }
    }
}
