/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import connexion.Connexion;
import java.lang.reflect.Field;
import java.sql.Connection;
import java.sql.Statement;
import java.util.Vector;

/**
 *
 * @author nyamp
 */
public class Insert {
    public void insert(Connection con,Object obj,Vector list) throws Exception {
        String tab = null;
        Statement stmt = null;
        int insert = 0;
        if(con==null) {
            con = new Connexion().getConnection();
        }
        try {
            String tablename = obj.getClass().getSimpleName();
            Field[] field = obj.getClass().getDeclaredFields();
            Object[] table = list.toArray();
            
            String sql = "insert into " + tablename + " (";
            int a = 0;
            String nameField = null;
            for(a=1; a<field.length; a++) {
                nameField = field[a].getName();
                if(a!=field.length-1 && a!=0) {
                    sql = sql + nameField + ",";
                }
                else if(a==field.length-1) {
                    sql = sql + nameField + ")";
                }
            }
            sql = sql + " values (";
            int i = 0;
            int j = 0;
            String s = null;
                for(i=0; i<field.length; i++) {
                    s = field[i].getType().getName();
                    System.out.println(s);
                    if(i==0) {
                        sql = sql;
                    }
                    else if(i!=field.length-1 && i!=0) {
                        if(s.equalsIgnoreCase("java.lang.String") == true || s.equalsIgnoreCase("java.sql.Date") == true || s.equalsIgnoreCase("java.util.Date") == true || s.equalsIgnoreCase("java.sql.Time") == true) {
                            sql = sql + "'" + list.elementAt(j) + "'" + ",";
                            j++;
                        }
                        else {
                            sql = sql + list.elementAt(j) + ",";
                            j++;
                        }
                    }
                    else if (i==field.length-1){
                        if(s.equalsIgnoreCase("java.lang.String") == true || s.equalsIgnoreCase("java.sql.Date") == true || s.equalsIgnoreCase("java.util.Date") == true || s.equalsIgnoreCase("java.sql.Time") == true) {
                            sql = sql + "'" + list.elementAt(j) + "'" + ")";
                        }
                        else {
                            sql = sql + list.elementAt(j) + ")";
                        }
                    }
                }
            tab = sql;
            System.out.println(tab);
            
            stmt = con.createStatement();
            insert = stmt.executeUpdate(tab);
        }
        
        catch(Exception e) {
            System.out.println(e);
            throw e;
        }
        
        finally {
            if(stmt!=null) { stmt.close(); }
            if(con!=null) { con.close(); }
        }
    }
}
